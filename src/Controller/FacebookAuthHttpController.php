<?php

namespace Drupal\social_auth_facebook_api\Controller;

use Drupal\social_auth_decoupled\SocialAuthDecoupledTrait;
use Drupal\social_auth_decoupled\SocialAuthHttpInterface;
use Drupal\social_auth_facebook\Controller\FacebookAuthController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * Post login responses for Social Auth Google.
 */
class FacebookAuthHttpController extends FacebookAuthController implements SocialAuthHttpInterface {

  use SocialAuthDecoupledTrait;

  /**
   * {@inheritdoc}
   */
  public function authenticateUserByProfile(ResourceOwnerInterface $profile, $data) {
    // Check for email.
    if (!$profile->getEmail()) {
      throw new BadRequestHttpException($this->t('Facebook authentication failed. This site requires permission to get your email address.'));
    }

    return $this->userAuthenticatorHttp()
      ->authenticateUser($profile->getName(), $profile->getEmail(), $profile->getId(), $this->providerManager->getAccessToken(), $profile->getPictureUrl(), $data);
  }

}
